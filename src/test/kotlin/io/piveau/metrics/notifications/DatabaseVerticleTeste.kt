package io.piveau.metrics.notifications

import de.flapdoodle.embed.mongo.MongodStarter
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder
import de.flapdoodle.embed.mongo.config.Net
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.process.runtime.Network
import io.piveau.metrics.notifications.database.*
import io.piveau.metrics.notifications.utils.DBCollection
import io.piveau.metrics.notifications.utils.DEFAULT_MONGODB_SERVER_HOST
import io.piveau.metrics.notifications.utils.ENV_MONGODB_SERVER_HOST
import io.piveau.metrics.notifications.utils.ENV_MONGODB_SERVER_PORT
import io.piveau.utils.PiveauContext
import io.vertx.core.DeploymentOptions
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.MongoClient
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


@DisplayName("Database Verticle methods test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(VertxExtension::class)
class DatabaseVerticleTeste {
    private val piveauLog = PiveauContext("metrics-notifications", "database-test").log()

    private var dbClient: MongoClient = MongoClient.createShared(Vertx.vertx(), JsonObject())

    private val vertx: Vertx = Vertx.vertx()

    @BeforeAll
    fun setup(vertxTestContext: VertxTestContext): Unit {

        //register message codecs, this would normally happen in main verticle


        vertx.eventBus().registerDefaultCodec(GetAllMessage::class.java, GetAllMessageCodec())
        vertx.eventBus().registerDefaultCodec(OneCatalogueMessage::class.java, OneCatalogueMessageCodec())
        vertx.eventBus().registerDefaultCodec(ReplaceOrInsertOneMessage::class.java, ReplaceOrInsertOneMessageCodec())
        vertx.eventBus().registerDefaultCodec(BlacklistMessage::class.java, BlacklistMessageCodec())

        try {
            val host: String = DEFAULT_MONGODB_SERVER_HOST
            val port = 27027 // use non-default port to prevent conflicts when developing


            val starter = MongodStarter.getDefaultInstance()
            val mongodConfig = MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(Net(host, port, Network.localhostIsIPv6()))
                .build()

            starter.prepare(mongodConfig).start()

            val config = JsonObject()
                .put("host", host)
                .put("port", port)

            dbClient = MongoClient.createShared(vertx, config)

            assertNotNull(dbClient, "Database creation failed. No tests possible")

            val testConfig: JsonObject = JsonObject()
                .put(ENV_MONGODB_SERVER_HOST, host)
                .put(ENV_MONGODB_SERVER_PORT, port)
            vertx.deployVerticle(
                DatabaseVerticle::class.java.name,
                DeploymentOptions().setConfig(testConfig),
                vertxTestContext.completing()
            )


        } catch (e: Exception) {
            vertxTestContext.failNow(e)
        }


    }

    @AfterEach
    fun resetDB(testContext: VertxTestContext) {
        dbClient.dropCollection(DBCollection.USER.name) { asyncResult ->
            if (asyncResult.succeeded()) {
                dbClient.dropCollection(DBCollection.CATALOGUE.name) {
                    if (it.succeeded()) {
                        testContext.completeNow()
                    } else {
                        testContext.failNow(it.cause())
                    }
                }
            } else {
                testContext.failNow(asyncResult.cause())
            }
        }
    }


    @Test
    fun testCreateExistingEmptyBlacklist(testContext: VertxTestContext) {


        initDBTestUser(vertx).onFailure { testContext.failNow(it) }.onSuccess {

            vertx.eventBus().request<JsonObject>(
                DatabaseVerticle.ADDRESS_DB_BLACKLIST_CREATE,
                OneCatalogueMessage(DBCollection.USER, "govdata")
            ) { asyncResult ->
                if (asyncResult.succeeded()) {

                    dbClient.findOne(DBCollection.USER.name, JsonObject().put("catalogue", "govdata"), null) {
                        if (it.succeeded()) {
                            assertNotNull(it.result())
                            assert(it.result().getJsonArray("blacklist").isEmpty)
                            testContext.completeNow()
                        } else {
                            testContext.failNow(it.cause())
                        }
                    }

                } else {
                    testContext.failNow(asyncResult.cause())
                }
            }
        }

    }


    @Test
    fun testCreateExistingNonEmptyBlacklist(testContext: VertxTestContext) {


        dbClient.save(
            DBCollection.USER.name,
            JsonObject().put("catalogue", "govdata").put("blacklist", JsonArray().add("testuser@example.com"))
        ) { dbrequest ->
            if (dbrequest.succeeded()) {

                vertx.eventBus().request<JsonObject>(
                    DatabaseVerticle.ADDRESS_DB_BLACKLIST_CREATE,
                    OneCatalogueMessage(DBCollection.USER, "govdata")
                ) { asyncResult ->
                    if (asyncResult.succeeded()) {

                        dbClient.findOne(DBCollection.USER.name, JsonObject().put("catalogue", "govdata"), null) {
                            if (it.succeeded()) {
                                assertNotNull(it.result())
                                assert(!it.result().getJsonArray("blacklist").isEmpty)

                                assertNotNull(it.result().getJsonArray("blacklist").getString(0))
                                assert(it.result().getJsonArray("blacklist").getString(0).isNotEmpty())
                                assertEquals(it.result().getJsonArray("blacklist").getString(0), "testuser@example.com")
                                testContext.completeNow()
                            } else {
                                testContext.failNow(it.cause())
                            }
                        }

                    } else {
                        testContext.failNow(asyncResult.cause())
                    }
                }

            } else {
                testContext.failNow(dbrequest.cause())
            }
        }

    }

    @Test
    fun testCreateBlacklist(testContext: VertxTestContext) {


        vertx.eventBus().request<JsonObject>(
            DatabaseVerticle.ADDRESS_DB_BLACKLIST_CREATE,
            OneCatalogueMessage(DBCollection.USER, "govdata")
        ) { asyncResult ->
            if (asyncResult.succeeded()) {

                dbClient.findOne(DBCollection.USER.name, JsonObject().put("catalogue", "govdata"), null) {
                    if (it.succeeded()) {
                        assertNotNull(it.result())
                        assert(it.result().getJsonArray("blacklist").isEmpty)


                        testContext.completeNow()
                    } else {
                        testContext.failNow(it.cause())
                    }
                }

            } else {
                testContext.failNow(asyncResult.cause())
            }
        }
    }


    @Test
    fun testAddBlacklist(testContext: VertxTestContext) {


        initDBTestUser(vertx).onFailure { testContext.failNow(it) }.onSuccess {

            vertx.eventBus().request<JsonObject>(
                DatabaseVerticle.ADDRESS_DB_BLACKLIST_ADD,
                BlacklistMessage("govdata", "testuser@example.com")
            ) { asyncResult ->
                if (asyncResult.succeeded()) {

                    dbClient.findOne(DBCollection.USER.name, JsonObject().put("catalogue", "govdata"), null) {
                        if (it.succeeded()) {
                            assertNotNull(it.result())
                            assert(!it.result().getJsonArray("blacklist").isEmpty)
                            assertNotNull(it.result().getJsonArray("blacklist").getString(0))
                            assert(it.result().getJsonArray("blacklist").getString(0).isNotEmpty())
                            assertEquals(it.result().getJsonArray("blacklist").getString(0), "testuser@example.com")
                            testContext.completeNow()
                        } else {
                            testContext.failNow(it.cause())
                        }
                    }

                } else {
                    testContext.failNow(asyncResult.cause())
                }
            }
        }

    }

    @Test
    fun testRemoveBlacklist(testContext: VertxTestContext) {


        dbClient.save(
            DBCollection.USER.name,
            JsonObject().put("catalogue", "govdata").put("blacklist", JsonArray().add("testuser@example.com"))
        ) { dbrequest ->
            if (dbrequest.succeeded()) {
                vertx.eventBus().request<JsonObject>(
                    DatabaseVerticle.ADDRESS_DB_BLACKLIST_REMOVE,
                    BlacklistMessage( "govdata", "testuser@example.com")
                ) { asyncResult ->
                    if (asyncResult.succeeded()) {

                        dbClient.findOne(DBCollection.USER.name, JsonObject().put("catalogue", "govdata"), null) {
                            if (it.succeeded()) {
                                assertNotNull(it.result())
                                assert(it.result().getJsonArray("blacklist").isEmpty)

                                testContext.completeNow()
                            } else {
                                testContext.failNow(it.cause())
                            }
                        }

                    } else {
                        testContext.failNow(asyncResult.cause())
                    }
                }
            } else {
                testContext.failNow(dbrequest.cause())
            }
        }
    }


    @Test
    fun testRemoveBlacklistWithMultipleEntries(testContext: VertxTestContext) {


        dbClient.save(
            DBCollection.USER.name,
            JsonObject().put("catalogue", "govdata")
                .put("blacklist", JsonArray().add("testuser@example.com").add("testuser2@example.com"))
        ) { dbrequest ->
            if (dbrequest.succeeded()) {
                vertx.eventBus().request<JsonObject>(
                    DatabaseVerticle.ADDRESS_DB_BLACKLIST_REMOVE,
                    BlacklistMessage( "govdata", "testuser@example.com")
                ) { asyncResult ->
                    if (asyncResult.succeeded()) {

                        dbClient.findOne(DBCollection.USER.name, JsonObject().put("catalogue", "govdata"), null) {
                            if (it.succeeded()) {
                                assertNotNull(it.result())
                                assert(!it.result().getJsonArray("blacklist").isEmpty)
                                assert(it.result().getJsonArray("blacklist").size() == 1)
                                assertNotNull(it.result().getJsonArray("blacklist").getString(0))
                                assert(it.result().getJsonArray("blacklist").getString(0).isNotEmpty())
                                assertEquals(
                                    it.result().getJsonArray("blacklist").getString(0),
                                    "testuser2@example.com"
                                )

                                testContext.completeNow()
                            } else {
                                testContext.failNow(it.cause())
                            }
                        }

                    } else {
                        testContext.failNow(asyncResult.cause())
                    }
                }
            } else {
                testContext.failNow(dbrequest.cause())
            }
        }
    }


    @Test
    fun testInsert(testContext: VertxTestContext) {


        val metrics = vertx.fileSystem().readFileBlocking("currentMetrics.json").toJsonObject()


        vertx.eventBus().request<JsonObject>(
            DatabaseVerticle.ADDRESS_REPLACE_OR_INSERT_IN_DB,
            ReplaceOrInsertOneMessage(DBCollection.CATALOGUE, "govdata", metrics)
        ) { asyncResult ->
            if (asyncResult.succeeded()) {

                val result = asyncResult.result().body()
                assertNotNull(result)
                assert(result.getBoolean("success"))
                assertNotNull(result.getJsonObject("new").getJsonObject("info"))
                assertNotNull(result.getJsonObject("new").getJsonObject("info").getString("title"))
                assertEquals("GovData", result.getJsonObject("new").getJsonObject("info").getString("title"))


                testContext.completeNow()


            } else {
                testContext.failNow(asyncResult.cause())
            }
        }


    }


    @Test
    fun testReplace(testContext: VertxTestContext) {


        initDBTestCatalogue(vertx).onFailure { testContext.failNow(it) }.onSuccess {


            val metrics = vertx.fileSystem().readFileBlocking("currentMetrics.json").toJsonObject()
            metrics.getJsonObject("info").put("title", "replaced")

            vertx.eventBus().request<JsonObject>(
                DatabaseVerticle.ADDRESS_REPLACE_OR_INSERT_IN_DB,
                ReplaceOrInsertOneMessage(DBCollection.CATALOGUE, "govdata", metrics)
            ) { asyncResult ->
                if (asyncResult.succeeded()) {
                    val result = asyncResult.result().body()


                    assertNotNull(result)
                    assert(result.getBoolean("success"))
                    assertNotNull(result.getJsonObject("new").getJsonObject("info"))
                    assertNotNull(result.getJsonObject("new").getJsonObject("info").getString("title"))
                    assertEquals("replaced", result.getJsonObject("new").getJsonObject("info").getString("title"))


                    testContext.completeNow()


                } else {
                    testContext.failNow(asyncResult.cause())
                }
            }
        }

    }


    @Test
    fun testGetAllCatalogues(testContext: VertxTestContext) {


        initDBTestCatalogue(vertx).onFailure { testContext.failNow(it) }.onSuccess {

            vertx.eventBus().request<JsonArray>(
                DatabaseVerticle.ADDRESS_GETALL_FROM_DB,
                GetAllMessage(DBCollection.CATALOGUE)
            ) { asyncResult ->
                if (asyncResult.succeeded()) {
                    val result = asyncResult.result().body()

                    assertNotNull(result)
                    assert(!result.isEmpty)
                    assert(result.size() == 1)
                    assert(result.getJsonObject(0) != null)
                    assert(!result.getJsonObject(0).isEmpty)
                    val cat = result.getJsonObject(0)


                    assertNotNull(cat.getJsonObject("info"))
                    assertNotNull(cat.getJsonObject("info").getString("title"))
                    assertEquals("GovData", cat.getJsonObject("info").getString("title"))

                    testContext.completeNow()

                } else {
                    testContext.failNow(asyncResult.cause())
                }
            }
        }

    }


    @Test
    fun testGetOneCatalogue(testContext: VertxTestContext) {


        initDBTestCatalogue(vertx).onFailure { testContext.failNow(it) }.onSuccess {

            vertx.eventBus().request<JsonArray>(
                DatabaseVerticle.ADDRESS_GETONE_FROM_DB,
                OneCatalogueMessage(DBCollection.CATALOGUE, "govdata")
            ) { asyncResult ->
                if (asyncResult.succeeded()) {
                    val result = asyncResult.result().body()

                    assertNotNull(result)
                    assert(!result.isEmpty)
                    assert(result.size() == 1)
                    assert(result.getJsonObject(0) != null)
                    assert(!result.getJsonObject(0).isEmpty)
                    val cat = result.getJsonObject(0)


                    assertNotNull(cat.getJsonObject("info"))
                    assertNotNull(cat.getJsonObject("info").getString("title"))
                    assertEquals("GovData", cat.getJsonObject("info").getString("title"))

                    testContext.completeNow()

                } else {
                    testContext.failNow(asyncResult.cause())
                }
            }
        }

    }

    @Test
    fun testGetOneUser(testContext: VertxTestContext) {



        dbClient.save(
            DBCollection.USER.name,
            JsonObject().put("catalogue", "govdata").put("blacklist", JsonArray().add("testuser@example.com"))
        ) { dbrequest ->
            if (dbrequest.succeeded()) {
                vertx.eventBus().request<JsonArray>(
                    DatabaseVerticle.ADDRESS_GETONE_FROM_DB,
                    OneCatalogueMessage(DBCollection.USER, "govdata")
                ) { asyncResult ->
                    if (asyncResult.succeeded()) {
                        val result = asyncResult.result().body()

                        assertNotNull(result)
                        assert(!result.isEmpty)
                        assert(result.size() == 1)
                        assert(result.getJsonObject(0) != null)
                        assert(!result.getJsonObject(0).isEmpty)
                        val cat = result.getJsonObject(0)


                        assertNotNull(cat.getString("catalogue"))
                        assertEquals("govdata", cat.getString("catalogue"))

                        assertNotNull(cat.getJsonArray("blacklist"))
                        assert(cat.getJsonArray("blacklist").size()==1)
                        assertEquals("testuser@example.com",cat.getJsonArray("blacklist").getString(0))

                        testContext.completeNow()

                    } else {
                        testContext.failNow(asyncResult.cause())
                    }
                }



            } else {
                testContext.failNow(dbrequest.cause())
            }
        }


    }



    private fun initDBTestUser(vertx: Vertx): Future<Void> = Promise.promise<Void>().apply {


        dbClient.save(
            DBCollection.USER.name,
            JsonObject().put("catalogue", "govdata").put("blacklist", JsonArray())
        ) { asyncResult ->
            if (asyncResult.succeeded()) {
                this.complete()
            } else {
                this.fail(asyncResult.cause())
            }
        }

    }.future()

    private fun initDBTestCatalogue(vertx: Vertx): Future<Void> = Promise.promise<Void>().apply {


        val metrics = vertx.fileSystem().readFileBlocking("currentMetrics.json").toJsonObject()
        dbClient.save(DBCollection.CATALOGUE.name, metrics) {
            if (it.succeeded()) {
                this.complete()
            } else {
                this.fail(it.cause())
            }

        }


    }.future()
}