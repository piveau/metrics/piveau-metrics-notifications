# Piveau metrics notifcation service

This is a service, which will notify a data provider per mail, when their data has a worse score, than at a specified
time interval before.

## Table of Contents

1. [Build](#build)
2. [Run](#run)
3. [Docker](#docker)
4. [API](#api)
5. [Testing](#testing)
6. [Configuration](#configuration)
    1. [Environment](#environment)
7. [License](#license)

## Build

Requirements:

* Git
* Maven 3
* Java 11
* MongoDB

```bash
$ git clone ...
$ mvn package
```

## Run

```bash
$ java -jar target/metrics-notifications-fat.jar
```

## Docker

Build docker image:

```bash
$ docker build -t piveau/metrics-notifications .
```

Run docker image:

```bash
$ docker run -it -p 8080:8080 piveau/metrics-notifications
```

## API

A formal OpenAPI 3 specification can be found in the `src/main/resources/webroot/openapi.yaml` file.
A visually more appealing version is available at `{url}:{port}` once the application has been started.

## Testing

For test purposes, a check for a single catalogue can be triggered manually.
The endpoint for that has the id `checkNowSingle` and can be accessed under the following
path: `/notification/check/now/{catalogueId}`.
This endpoint ignores the list of enabled catalogues and will always check the catalogue with the given id.

This endpoint has two optional query parameters:

* `comparedScore`: The score that is compared to the score of the catalogue. This is useful to force triggering a
  notification for a catalogue with a score lower than this value.
* `mailreceiver`: The mail address of the user that will receive the notification. This will overwrite the mail address
  of the user that owns the catalogue. Thus the mail will then only be sent to the mail specified here. Should be
  urlencoded.

## Configuration

### Environment

| Key                             | Description                                                                                                                                                                                                                                                                                                                                                                                                                           | Default                              |
|:--------------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------------|
| NOTIFICATIONS_PORT              | Port this service will run on                                                                                                                                                                                                                                                                                                                                                                                                         | 8080                                 |
| NOTIFICATIONS_APIKEY            | **Mandatory**: The Apikey to refresh and clear the Cache.                                                                                                                                                                                                                                                                                                                                                                             |                                      |
| NOTIFICATIONS_ACTIVE_CATALOGUES | Whitelist for Catalogues: Only the catalogues in this Json Array will be checked                                                                                                                                                                                                                                                                                                                                                      | []                                   |
| NOTIFICATIONS_TIMER_UNIT        | The unit the timer considers for the next run day. Possible Options: WEEK, MONTH                                                                                                                                                                                                                                                                                                                                                      | WEEK                                 |
| NOTIFICATIONS_TIMER_COUNT       | The days of the timer unit the timer runs, seperated by comma. Greater days than possible for a unit will be ignored. E.g. 3,8 for Week: Timer runs on Every Wednesday                                                                                                                                                                                                                                                                | 1                                    |
| NOTIFICATIONS_TIMER_HOUR        | The time the timer runs on a run day. Only the hour of the day can be specified.                                                                                                                                                                                                                                                                                                                                                      | 9                                    |
| NOTIFICATIONS_CORS_DOMAINS      | Domains from which CORS access should be allowed as Json Array. Example value: `["localhost","example.com"] `                                                                                                                                                                                                                                                                                                                         |                                      |
| MAIL_SERVER_CONFIG              | Configuration for the connection with a mail server. JsonObject according to [Vert.X MailConfig](https://vertx.io/docs/apidocs/io/vertx/ext/mail/MailConfig.html#MailConfig-java.lang.String-int-io.vertx.ext.mail.StartTLSOptions-io.vertx.ext.mail.LoginOption-) Example:``` {"hostname": "<smtp_host>", "port": "<smtp_port>", ...} ```. `smtp_domain` will be used as sender domain, if no from address is explicitly configured. |                                      |
| MAIL_FROM_ADDRESS               | Mail address for the sender. Overwrites all other configuration for it.                                                                                                                                                                                                                                                                                                                                                               | no-reply@example.com                 |
| MONGODB_SERVER_HOST             | Hostname this service will try to connect to for the mongo db                                                                                                                                                                                                                                                                                                                                                                         | localhost                            |
| MONGODB_SERVER_PORT             | Port this service will try to connect to for the mongo db                                                                                                                                                                                                                                                                                                                                                                             | 27017                                |
| MONGODB_USERNAME                | Username this service will use for communicating with the Mongo DB                                                                                                                                                                                                                                                                                                                                                                    |                                      |
| MONGODB_PASSWORD                | Password this service will use for communicating with the Mongo DB                                                                                                                                                                                                                                                                                                                                                                    |                                      |
| MONGODB_DB_NAME                 | Database name this service will use for communicating with the Mongo DB                                                                                                                                                                                                                                                                                                                                                               | notifications                        |
| PIVEAU_TRIPLESTORE_CONFIG       | config for triplestore access. Refer to piveau-utils documentation.                                                                                                                                                                                                                                                                                                                                                                   | Refer to piveau-utils documentation. |
| TEST_ENVIRONMENT                | To prevent accidential mail sending, this service is by default in a test mode. If you want to send mails, set this to false.                                                                                                                                                                                                                                                                                                         | true                                 |
| smtp_host                       | hostname of the mailserver, will overwrite `hostname` in `MAIL_SERVER_CONFIG`.                                                                                                                                                                                                                                                                                                                                                        | true                                 |
| smtp_user                       | username for the login, will overwrite `username` in `MAIL_SERVER_CONFIG`.                                                                                                                                                                                                                                                                                                                                                            | true                                 |
| smtp_pwd                        | password for the login, will overwrite `password` in `MAIL_SERVER_CONFIG`.                                                                                                                                                                                                                                                                                                                                                            | true                                 |
| smtp_domain                     | the hostname to be used for HELO/ EHLO and the Message-ID, will overwrite `hostname` in `MAIL_SERVER_CONFIG`. Will be used as domain for the from address if `MAIL_FROM_ADDRESS` is not set.                                                                                                                                                                                                                                          | true                                 |

## License

[Apache License, Version 2.0](LICENSE.md)
