# Note: An invalid OpenAPI 3 yaml file will cause the following exception at startup:
# io.vertx.ext.web.api.contract.RouterFactoryException: Wrong specification url/path: webroot/openapi.yaml


# A Swagger 3.0 (a.k.a. OpenAPI) definition of the Engine API.
#
# This is used for generating API documentation and the types used by the
# client/server. See api/README.md for more information.
#
# Some style notes:
# - This file is used by ReDoc, which allows GitHub Flavored Markdown in
#   descriptions.
# - There is no maximum line length, for ease of editing and pretty diffs.
# - operationIds are in the format "verbNoun".

#Some Comments are copied from the Docker Engine API Specification

openapi: 3.0.3


info:
  version: ${project.version}
  title: metrics-notifications
  x-logo:
    url: "images/logo"



# The tags on paths define the menu sections in the ReDoc documentation, so
# the usage of tags must make sense for that:
# - They should be singular, not plural.
# - There should not be too many tags, or the menu becomes unwieldly. For
#   example, it is preferable to add a path to the "System" tag instead of
#   creating a tag with a single path in it.
# - The order of tags in this list defines the order in the menu.
tags:
  # Primary objects
  - name: Health
    x-displayName: "Health"
    description: |
      Show service health status.
  - name: User
    x-displayName: "User"
    description: |
      Endpoints for user interaction.
  - name: Notification
    x-displayName: "Notification"
    description: |
      Endpoints for controlling the service.

paths:
  /health:
    get:
      description: Show this services health status
      summary: Show Health status
      operationId: healthCheck
      tags:
        - Health
      responses:
        '200':
          description: Request accepted.
          content:
            application/json:
              schema:
                type: string
        '400':
          description: Invalid request.

  /user/{mail}/blacklist/{catalogue}:
    put:
      tags:
        - User
      summary: Set user on mailing blacklist
      description: Unsubscribe user from getting notifications for a catalogue.
      operationId: unsubscribeFromCatalogue
      parameters:
        - name: mail
          in: path
          description: User email
          required: true
          schema:
            type: string
            format: email
        - name: catalogue
          in: path
          description: Catalogue to unsubscribe from
          required: true
          schema:
            type: string
      security:
        - ApiKeyAuth: []
      responses:
        200:
          description: Unsubscribe successful
        400:
          description: Bad request
        500:
          description: Internal server error
    delete:
      tags:
        - User
      summary: Remove uer from mailing blacklist
      description: Subscribe user to getting notifications for a catalogue.
      operationId: subscribeToCatalogue
      parameters:
        - name: mail
          in: path
          description: User email
          required: true
          schema:
            type: string
            format: email
        - name: catalogue
          in: path
          description: Catalogue to subscribe to
          required: true
          schema:
            type: string
      security:
        - ApiKeyAuth: []
      responses:
        200:
          description: subscribe successful
        400:
          description: Bad request
        500:
          description: Internal server error

  /notification/check/now:
    post:
      tags:
        - Notification
      summary: Start a check for now
      description: Create a score check process that starts right now.
      operationId: checkNow
      parameters:
        - name: lastScore
          in: query
          description: Sets the last score that the new score is checked against. when setting to a negative number, this setting will be ignored. Default is -1
          required: false
          schema:
            default: -1
            type: integer
      security:
        - ApiKeyAuth: []
      responses:
        202:
          description: Accepted Request and started async process
        400:
          description: Bad request
        500:
          description: Internal server error

  /notification/check/now/{catalogueId}:
    post:
      tags:
        - Notification
      summary: Start a check for a single catalogue
      description: Create a score check process that starts right now and checks a specific catalogue.
      operationId: checkNowSingle
      parameters:
        - name: catalogueId
          in: path
          description: id of the catalogue that should be checked
          required: true
          schema:
            type: string
        - name: comparedScore
          in: query
          description: Sets the last score that the new score is compared against. when setting to a negative number, this setting will be ignored. Default is -1
          required: false
          schema:
            default: -1
            type: integer
        - name: mailreceiver
          in: query
          description: Sets the email address that the notification should be sent to. when setting to an empty string, the adress from the catalogue publisher will be used, which is also default for all scheduled checks.
          required: false
          schema:
            type: string
      security:
        - ApiKeyAuth: []
      responses:
        202:
          description: Accepted Request and started async process
        400:
          description: Bad request
        500:
          description: Internal server error
components:
  securitySchemes:
    ApiKeyAuth:
      type: apiKey
      in: header
      name: Authorization
