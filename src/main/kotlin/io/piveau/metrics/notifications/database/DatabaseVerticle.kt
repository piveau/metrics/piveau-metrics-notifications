package io.piveau.metrics.notifications.database

import io.piveau.metrics.notifications.utils.*
import io.piveau.utils.PiveauContext
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.FindOptions
import io.vertx.ext.mongo.MongoClient
import io.vertx.ext.mongo.UpdateOptions
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.awaitResult

/**
 * Database verticle
 * For translating the messages from the Notification Verticle into database queries for mongoDB
 *
 */
class DatabaseVerticle : CoroutineVerticle() {
    private lateinit var dbClient: MongoClient

    private val piveauLog = PiveauContext("metrics.notifications", "database").log()

    companion object {
        const val ADDRESS_GETALL_FROM_DB: String = "io.piveau.metrics.notifications.db.get_all"
        const val ADDRESS_GETONE_FROM_DB: String = "io.piveau.metrics.notifications.db.get_one"
        const val ADDRESS_REPLACE_OR_INSERT_IN_DB: String = "io.piveau.report.metrics.notifications.db.replace_insert"
        const val ADDRESS_DB_BLACKLIST_ADD: String = "io.piveau.report.metrics.notifications.db.blacklist.add"
        const val ADDRESS_DB_BLACKLIST_REMOVE: String = "io.piveau.report.metrics.notifications.db.blacklist.remove"
        const val ADDRESS_DB_BLACKLIST_CREATE: String = "io.piveau.report.metrics.notifications.db.blacklist.create"
    }


    /**
     * Start
     *
     */
    override suspend fun start() {
        piveauLog.info("Starting DatabaseVerticle")
        vertx.eventBus().consumer(ADDRESS_GETALL_FROM_DB, this::handleGetAll)
        vertx.eventBus().consumer(ADDRESS_GETONE_FROM_DB, this::handleGetOne)
        vertx.eventBus().consumer(ADDRESS_REPLACE_OR_INSERT_IN_DB, this::handleReplaceOrInsert)
        //User specific
        vertx.eventBus().consumer(ADDRESS_DB_BLACKLIST_ADD, this::handleBlacklistAdd)
        vertx.eventBus().consumer(ADDRESS_DB_BLACKLIST_REMOVE, this::handleBlacklistRemove)
        vertx.eventBus().consumer(ADDRESS_DB_BLACKLIST_CREATE, this::handleBlacklistCreate)

        val mongoDbConfig: JsonObject = JsonObject()
            .put("serverSelectionTimeoutMS", 1000)
            .put("host", config.getString(ENV_MONGODB_SERVER_HOST, DEFAULT_MONGODB_SERVER_HOST))
            .put("port", config.getInteger(ENV_MONGODB_SERVER_PORT, DEFAULT_MONGODB_SERVER_PORT))
            .put("db_name", config.getString(ENV_MONGODB_DB_NAME, DEFAULT_MONGODB_DB_NAME))
            .put("username", config.getString(ENV_MONGODB_USERNAME, null))
            .put("password", config.getString(ENV_MONGODB_PASSWORD, null))

        piveauLog.info("MongoDB config: {}", mongoDbConfig.encodePrettily())
        //establish db connection
        dbClient = MongoClient.createShared(vertx, mongoDbConfig)
        piveauLog.info("Successfully created database connection")
    }


    /**
     * Get all documents of a certain collection, User or Catalogue
     *
     * @param message
     */
    private fun handleGetAll(message: Message<GetAllMessage>) {
        piveauLog.info("handleGetAll; Received message: {}", message.body())
        val body = message.body()
        dbClient.find(body.collection.name, JsonObject()) { asyncResult ->
            if (asyncResult.succeeded()) {
                val retval = JsonArray().apply { asyncResult.result().iterator().forEach { add(it) } }
                message.reply(retval)
            } else {
                message.fail(400, asyncResult.cause().message)
            }
        }
    }

    /**
     * Get one document of a certain collection, User or Catalogue
     *
     * @param message
     */
    private fun handleGetOne(message: Message<OneCatalogueMessage>) {

        val body = message.body()
        val query = JsonObject().apply {
            if (body.collection == DBCollection.USER) {
                put("catalogue", body.catalogue)
            }
            if (body.collection == DBCollection.CATALOGUE) {
                put("info.id", body.catalogue)
            }

        }

        dbClient.find(body.collection.name, query) { asyncResult ->
            if (asyncResult.succeeded()) {
                val retval = JsonArray().apply { asyncResult.result().iterator().forEach { add(it) } }
                message.reply(retval)
            } else {
                message.fail(400, asyncResult.cause().message)
            }
        }
    }


    /**
     * Replace or insert a single document
     *
     * @param message
     */
    private fun handleReplaceOrInsert(message: Message<ReplaceOrInsertOneMessage>) {


        val body = message.body()

        //piveauLog.info(body.encodePrettily())

        dbClient.findOneAndReplaceWithOptions(
            body.collection.name,
            JsonObject().put("info.id", body.catalogue),
            body.document,
            FindOptions(),
            UpdateOptions().setUpsert(true).setReturningNewDocument(true)
        ) {
            if (it.succeeded()) {
                message.reply(JsonObject().put("success", true).put("new", it.result()))
            } else {
                message.fail(400, it.cause().message)
            }
        }


    }


    /**
     * Create empty blacklist for this catalogue, if "collection" is Catalogue, this is an error
     *
     * @param message
     */
    private fun handleBlacklistCreate(message: Message<OneCatalogueMessage>) {

        val body = message.body()
        val update = JsonObject().put("\$setOnInsert", JsonObject().put("blacklist", JsonArray()))

        if (body.collection == DBCollection.USER) {
            dbClient.findOneAndUpdateWithOptions(
                body.collection.name,
                JsonObject().put("catalogue", body.catalogue),
                update,
                FindOptions(),
                UpdateOptions().setUpsert(true).setReturningNewDocument(true)
            ) {
                if (it.succeeded()) {
                    piveauLog.info(it.result().encodePrettily())
                    if (it.result().getJsonArray("blacklist") != null) {
                        message.reply(JsonObject().put("success", true))
                    } else {
                        message.fail(500, "Could not update database")
                    }
                } else {
                    message.fail(500, it.cause().message)
                }
            }

        } else {
            message.fail(500, "CATALOGUE cannot be handled")

        }

    }


    /**
     * Add mail to blacklist, if "collection" is Catalogue, this is an error
     *
     * @param message
     */
    private fun handleBlacklistAdd(message: Message<BlacklistMessage>) {

        val body = message.body()
        val update = JsonObject().put("\$addToSet", JsonObject().put("blacklist", body.mail))
        userBlacklistModify(body, update, true).onFailure { message.fail(500, it.message) }
            .onSuccess { message.reply(it) }

    }


    /**
     * Remove mail from blacklist
     * Collection `CATALOGUE` is not handled yet.
     *
     * @param message
     */
    private fun handleBlacklistRemove(message: Message<BlacklistMessage>) {
        val body = message.body()
        val update = JsonObject().put("\$pull", JsonObject().put("blacklist", body.mail))
        userBlacklistModify(body, update, false).onFailure { message.fail(500, it.message) }
            .onSuccess { message.reply(it) }

    }

    /**
     * User blacklist modify modifies the mailing blacklist according to the update Object
     *
     * @param message containing the catalogue to be updated and the mail
     * @param update contains the mongodb update instructions
     * @return
     */
    private fun userBlacklistModify(message: BlacklistMessage, update: JsonObject, add: Boolean): Future<JsonObject> =
        Promise.promise<JsonObject>().apply {

            piveauLog.debug("Blacklist add: $add, message: $message, updateObject: $update")


            dbClient.findOneAndUpdateWithOptions(
                DBCollection.USER.name,
                JsonObject().put("catalogue", message.catalogue),
                update,
                FindOptions(),
                UpdateOptions().setUpsert(true).setReturningNewDocument(true)
            ) {
                if (it.succeeded()) {
                    piveauLog.info(it.result().encodePrettily())
                    if (it.result().getJsonArray("blacklist").contains(message.mail) == add) {
                        complete(JsonObject().put("success", true))
                    } else {
                        fail("Could not update database")
                    }
                } else {
                    fail(it.cause().message)
                }
            }


        }.future()

}