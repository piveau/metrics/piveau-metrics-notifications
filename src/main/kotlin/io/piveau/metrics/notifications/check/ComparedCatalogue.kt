package io.piveau.metrics.notifications.check

/**
 * Compared percentage has the info on the current value and how it compares to the last time,
 * if it went up or down or neither
 *
 * @property value
 * @property down
 * @property up
 * @constructor Create empty Compared percentage
 */
data class ComparedPercentage(var value: Int? = null, var down: Boolean = false, var up: Boolean = false)

/**
 * Compared status code, for the status code measurements,
 * has the info on the current value and how it compares to the last time,
 * if it went up or down or neither
 *
 * @property value
 * @property down
 * @property up
 * @constructor Create empty Compared status code
 */
data class ComparedStatusCode(var value: String? = null, var down: Boolean = false, var up: Boolean = false)

/**
 * Accessibility
 *
 * @property accessUrlStatusCodes
 * @property downloadUrlStatusCodes
 * @property downloadUrlAvailability
 * @constructor Create empty Accessibility
 */
data class Accessibility(
    var accessUrlStatusCodes: ComparedStatusCode = ComparedStatusCode(),
    var downloadUrlStatusCodes: ComparedStatusCode = ComparedStatusCode(),
    var downloadUrlAvailability: ComparedPercentage = ComparedPercentage(),
)

/**
 * Contextuality
 *
 * @property rightsAvailability
 * @property byteSizeAvailability
 * @property dateIssuedAvailability
 * @property dateModifiedAvailability
 * @constructor Create empty Contextuality
 */
data class Contextuality(
    var rightsAvailability: ComparedPercentage = ComparedPercentage(),
    var byteSizeAvailability: ComparedPercentage = ComparedPercentage(),
    var dateIssuedAvailability: ComparedPercentage = ComparedPercentage(),
    var dateModifiedAvailability: ComparedPercentage = ComparedPercentage(),
)

/**
 * Findability
 *
 * @property keywordAvailability
 * @property categoryAvailability
 * @property spatialAvailability
 * @property temporalAvailability
 * @constructor Create empty Findability
 */
data class Findability(
    var keywordAvailability: ComparedPercentage = ComparedPercentage(),
    var categoryAvailability: ComparedPercentage = ComparedPercentage(),
    var spatialAvailability: ComparedPercentage = ComparedPercentage(),
    var temporalAvailability: ComparedPercentage = ComparedPercentage(),
)

/**
 * Interoperability
 *
 * @property formatAvailability
 * @property mediaTypeAvailability
 * @property formatMediaTypeAlignment
 * @property formatMediaTypeNonProprietary
 * @property formatMediaTypeMachineReadable
 * @property dcatApCompliance
 * @constructor Create empty Interoperability
 */
data class Interoperability(
    var formatAvailability: ComparedPercentage = ComparedPercentage(),
    var mediaTypeAvailability: ComparedPercentage = ComparedPercentage(),
    var formatMediaTypeAlignment: ComparedPercentage = ComparedPercentage(),
    var formatMediaTypeNonProprietary: ComparedPercentage = ComparedPercentage(),
    var formatMediaTypeMachineReadable: ComparedPercentage = ComparedPercentage(),
    var dcatApCompliance: ComparedPercentage = ComparedPercentage(),
)

/**
 * Reusability
 *
 * @property licenceAvailability
 * @property licenceAlignment
 * @property accessRightsAvailability
 * @property accessRightsAlignment
 * @property contactPointAvailability
 * @property publisherAvailability
 * @constructor Create empty Reusability
 */
data class Reusability(
    var licenceAvailability: ComparedPercentage = ComparedPercentage(),
    var licenceAlignment: ComparedPercentage = ComparedPercentage(),
    var accessRightsAvailability: ComparedPercentage = ComparedPercentage(),
    var accessRightsAlignment: ComparedPercentage = ComparedPercentage(),
    var contactPointAvailability: ComparedPercentage = ComparedPercentage(),
    var publisherAvailability: ComparedPercentage = ComparedPercentage(),
)


/**
 * Compared catalogue contains one Catalogue with current values which were compared to a historic version.
 * It is indicated, if the new values went up or down or neither, so that this object can be used
 * in a thymeleaf template to display values that changed
 *
 * @property accessibility
 * @property contextuality
 * @property findability
 * @property interoperability
 * @property reusability
 * @constructor Create empty Compared catalogue
 */
data class ComparedCatalogue(
    var accessibility: Accessibility = Accessibility(),
    var contextuality: Contextuality = Contextuality(),
    var findability: Findability = Findability(),
    var interoperability: Interoperability = Interoperability(),
    var reusability: Reusability = Reusability()
)


data class MailList(val mails: List<String>, val dcat:Boolean=false, val name:String="")