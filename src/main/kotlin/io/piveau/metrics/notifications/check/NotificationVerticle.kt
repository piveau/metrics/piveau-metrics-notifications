package io.piveau.metrics.notifications.check


import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.json.ConfigHelper
import io.piveau.metrics.notifications.database.DatabaseVerticle
import io.piveau.metrics.notifications.database.GetAllMessage
import io.piveau.metrics.notifications.database.OneCatalogueMessage
import io.piveau.metrics.notifications.database.ReplaceOrInsertOneMessage
import io.piveau.metrics.notifications.utils.*
import io.piveau.utils.PiveauContext
import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.circuitbreaker.CircuitBreakerOptions
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.Message
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.mail.*
import io.vertx.ext.web.client.HttpRequest
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.apache.jena.query.QuerySolution
import org.apache.jena.sparql.vocabulary.FOAF
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms
import org.apache.jena.vocabulary.SKOS
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeParseException
import java.time.temporal.ChronoUnit
import kotlin.streams.asSequence


class NotificationVerticle : CoroutineVerticle() {

    private val piveauContext = PiveauContext("metrics.notifications", "notification")
    private val piveauLog = piveauContext.log()

    private val IANATYPE_APPLICATION_JSON = "application/json"
    private val IANATYPE_IMAGE_PNG = "image/png"
    private val IANATYPE_IMAGE_SVG = "image/svg+xml"

    private lateinit var client: WebClient

    private lateinit var breaker: CircuitBreaker

    private lateinit var address: String
    private lateinit var historicCataloguesRequest: HttpRequest<Buffer>
    private lateinit var currentCataloguesRequest: HttpRequest<Buffer>

    private val scoredelta: Int by lazy { config.getInteger(ENV_SCORE_DELTA, DEFAULT_SCORE_DELTA) }
    private val catalogueBaseUrl: String by lazy {
        val url = config.getString(
            EVN_CATALOGUE_DETAIL_BASE_URL,
            DEFAULT_CATALOGUE_DETAIL_BASE_URL
        )
        if (url.endsWith("/")) {
            url
        } else {
            "$url/"
        }
    }

    private val mailCopyTo: JsonArray by lazy {
        ConfigHelper.forConfig(config).forceJsonArray(ENV_MAIL_COPY_TO)
    }

    private val activeCatalogues: JsonArray by lazy {
        ConfigHelper.forConfig(config).forceJsonArray(ENV_ACTIVE_CATALOGUES)
    }

    private lateinit var tripleStore: TripleStore

    private lateinit var mailClient: MailClient

    private lateinit var logo: Buffer
    private lateinit var arrowup: Buffer
    private lateinit var arrowdown: Buffer
    private lateinit var fromAddress: String

    private val templateEngine: TemplateEngine = TemplateEngine()


    companion object {
        const val ADDRESS_NOTIFY_CHECK: String = "io.piveau.metrics.notifications.timer.run"
    }

    override suspend fun start() {
        piveauLog.info("Starting NotificationVerticle")
        vertx.eventBus().consumer(ADDRESS_NOTIFY_CHECK, this::handleNotifyCheck)

        client = WebClient.create(vertx)
        breaker = CircuitBreaker.create(
            "metrics-breaker", vertx, CircuitBreakerOptions()
                .setMaxRetries(10)
                .setTimeout(config.getLong("circuitBreakerTimeout", -1))
        ).retryPolicy { t, c ->
            piveauContext.extend("metrics-breaker").log().debug("Try number $c")
            c * 5000L
        }

        val configHelper = ConfigHelper.forConfig(config)

        tripleStore = TripleStore(vertx, configHelper.forceJsonObject("PIVEAU_TRIPLESTORE_CONFIG"))

        //metrics cache setup
        address = config.getString(ENV_METRICS_ADDRESS, DEFAULT_METRICS_ADDRESS)

        historicCataloguesRequest = client.getAbs("$address/catalogues/history")
            .putHeader(HttpHeaders.ACCEPT.toString(), IANATYPE_APPLICATION_JSON)

        currentCataloguesRequest = client.getAbs("$address/catalogues")
            .putHeader(HttpHeaders.ACCEPT.toString(), IANATYPE_APPLICATION_JSON)

        //Timer setup
        val timerUnit = TimerUnit.valueOf(config.getString(ENV_TIMER_UNIT, DEFAULT_TIMER_UNIT))
        val timerCount =
            config.getString(ENV_TIMER_COUNT, DEFAULT_TIMER_COUNT).split(",").stream().map { it.toIntOrNull() }
                .asSequence().filterNotNull().toList()
        val runHour = config.getInteger(ENV_TIMER_HOUR, DEFAULT_TIMER_HOUR)

        val lastDate = calculateLastDate(LocalDateTime.now(), timerUnit, timerCount, runHour)

        piveauLog.info("Last date is $lastDate")
        if (lastDate != null) {
            startupCheck(lastDate)
        } else {
            piveauLog.error("Could not calculate last date for timer")

        }

        piveauLog.info("getting mail config")

        val mailJson = ConfigHelper.forConfig(config).forceJsonObject(ENV_MAIL_SERVER_CONFIG)
        config.getString("smtp_host")?.let { mailJson.put("hostname", it) }
        config.getString("smtp_user")?.let { mailJson.put("username", it) }
        config.getString("smtp_pwd")?.let { mailJson.put("password", it) }

        mailJson.getString("ownHostname")?.let {
            fromAddress = "no-reply@${it.trim()}"
        }

        config.getString("smtp_domain")?.let {
            mailJson.put("ownHostname", it.trim())
            fromAddress = "no-reply@${it.trim()}"
        }

        val envFromAddress= config.getString(ENV_MAIL_FROM_ADDRESS, "")
        if(envFromAddress.isNotBlank()) fromAddress = envFromAddress

        if(!this::fromAddress.isInitialized) fromAddress = DEFAULT_MAIL_FROM_ADDRESS


        //Mail & Mailtemplate setup
        val mailConfig = MailConfig(mailJson)

        mailConfig.isTrustAll = true
        piveauLog.debug("Mail config is: ${mailConfig.toJson().encodePrettily()}")

        mailClient = MailClient.createShared(vertx, mailConfig)

        logo = vertx.fileSystem().readFileBlocking("emails/logo.png")
        arrowdown = vertx.fileSystem().readFileBlocking("emails/down_white.png")
        arrowup = vertx.fileSystem().readFileBlocking("emails/up_white.png")

        val templateResolver = ClassLoaderTemplateResolver().also {
            it.templateMode = TemplateMode.HTML
            it.prefix = "/emails/"
            it.suffix = ".html"
        }

        templateEngine.setTemplateResolver(templateResolver)


        val schemaConfig = ConfigHelper.forConfig(config).forceJsonObject(ENV_PIVEAU_DCATAP_SCHEMA_CONFIG)
        if (schemaConfig.isEmpty) {
            piveauLog.error("No schema config found")
            schemaConfig.put(
                "baseUri",
                config.getString(
                    ENV_BASE_URI,
                    DCATAPUriSchema.DEFAULT_BASE_URI
                )
            )
        }
        DCATAPUriSchema.config = schemaConfig

        piveauLog.info("NotificationVerticle started")
    }


    /**
     * Startup check checks for missing catalogues and adds them to the internal db
     *
     * should be run on startup
     *
     * @param lastRunDate
     * @return
     */
    private fun startupCheck(lastRunDate: LocalDate): Future<List<JsonObject>> =
        Promise.promise<List<JsonObject>>().apply {
            piveauLog.info("active catalogues are: $activeCatalogues")
            getAllCataloguesFromDB()
                .onSuccess { list ->
                    if (list.isEmpty) {
                        piveauLog.info("List is empty, collecting from cache")
                        getHistoricCataloguesFromCache(lastRunDate)
                            .onSuccess { jsonArray ->
                                jsonArray
                                    .map { it as JsonObject }
                                    .filter {
                                        activeCatalogues.contains(
                                            it.getJsonObject("info", JsonObject()).getString("id", "")
                                        )
                                    }
                                    .forEach { catalogue ->
                                        val id = catalogue.getJsonObject("info", JsonObject()).getString("id")
                                        piveauLog.info("Catalogue id: $id")

                                        vertx.eventBus().send(
                                            DatabaseVerticle.ADDRESS_DB_BLACKLIST_CREATE,
                                            OneCatalogueMessage(DBCollection.USER, id)
                                        )

                                        replaceOrInsertCatalogueInDB(
                                            remorphDateArrays(catalogue),
                                            lastRunDate
                                        ).onFailure {
                                            piveauLog.warn(
                                                "could not load Catalogue into database ",
                                                it
                                            )
                                        }.onSuccess {
                                            piveauContext
                                                .extend(
                                                    it.getJsonObject("new", JsonObject())
                                                        .getJsonObject("info", JsonObject()).getString("id")
                                                ).log()
                                                .debug("Successfully loaded Catalogue into database")
                                        }

                                    }
                                complete(jsonArray.map { it as JsonObject }.toList())
                            }
                            .onFailure { fail(it.cause) }

                    } else {
                        complete(listOf())
                    }
                }
                .onFailure {
                    piveauLog.error("Failed to get collection from database", it)
                    fail(it.cause)
                }
        }.future()

    /**
     * Handle notify check. Called when the check is started.
     *
     * @param message a message containing the string of the date of the last time this check was run
     */
    private fun handleNotifyCheck(message: Message<JsonObject>) {

        piveauLog.info("NOTIFY")
        val last: LocalDateTime
        try {
            last = LocalDateTime.parse(message.body().getString("date"))
            piveauLog.info("Time parsed")
        } catch (e: DateTimeParseException) {
            piveauLog.error("Could not parse message '${message.body().getString("date")}' into LocalDateTime", e)
            return
        }
        piveauLog.info(ChronoUnit.MINUTES.between(last.toLocalTime(), LocalTime.now()).toString())

        val scoreCompare = message.body().getInteger("comparedScore")
        val catalogue = message.body().getString("catalogue", "")
        val mailreceiver = message.body().getString("mailreceiver", "")

        collectSendSet(last.toLocalDate(), catalogue, scoreCompare).onFailure { message.fail(500, it.message) }
            .onSuccess { jsonset ->


                val logoAttachement = MailAttachment.create()
                with(logoAttachement) {
                    contentType = IANATYPE_IMAGE_PNG
                    data = logo
                    disposition = "inline"
                    contentId = "logo"
                }

                val arrowUpAttachement = MailAttachment.create()
                with(arrowUpAttachement) {
                    contentType = IANATYPE_IMAGE_PNG
                    data = arrowup
                    disposition = "inline"
                    contentId = "arrowup"
                }

                val arrowDownAttachement = MailAttachment.create()
                with(arrowDownAttachement) {
                    contentType = IANATYPE_IMAGE_PNG
                    data = arrowdown
                    disposition = "inline"
                    contentId = "arrowdown"
                }

                piveauLog.info("send set size is ${jsonset.size}")
                jsonset.parallelStream().forEach { sendObject ->


                    val id = sendObject.getJsonObject("new")?.getJsonObject("info")?.getString("id")
                    val compared = getComparedData(sendObject.getJsonObject("old"), sendObject.getJsonObject("new"))



                    if (id != null) {
                        piveauContext.extend(id).log().info(compared.toString())
                    } else {
                        piveauLog.debug(compared.toString())
                        return@forEach
                    }

                    getMaillist(id, mailreceiver)
                        .onFailure {
                            piveauContext.extend(id).log().warn("Could not get info from Triplestore", it)
                            message.fail(500, "Could not get info from Triplestore: ${it.message}")
                        }
                        .onSuccess { mailListObj ->


                            val mailList = mailListObj.mails
                            val isDcat = mailListObj.dcat
                            val name = mailListObj.name.ifEmpty { "Publisher" }

                            // we do not have to do all this, if we dont have any mail to send
                            if (mailList.isNotEmpty()) {
                                piveauLog.info("Sending mail to ${mailList.size} recipients: ${mailList.joinToString()}")
                                piveauLog.info("testingScore is ${sendObject.getInteger("scoreCompare", -1)}")

                                val mailsubject = "Your data.europa.eu metadata quality score went down"
                                val ctx = Context()
                                val scoreBefore = sendObject.getJsonObject("old", JsonObject()).getInteger("score",0)
                                val scoreAfter = sendObject.getJsonObject("new", JsonObject()).getInteger("score",0)
                                ctx.setVariables(
                                    mapOf(
                                        "isDcat" to isDcat,
                                        "publisher" to name, // name is Publisher, if it is empty
                                        "logo" to "logo",
                                        "arrowup" to "arrowup",
                                        "arrowdown" to "arrowdown",
                                        "data" to compared,
                                        "catalogue" to id,
                                        "catalogueBaseUrl" to catalogueBaseUrl,
                                        "scoreBefore" to scoreBefore,
                                        "scoreAfter" to scoreAfter,
                                        "percentChange" to (scoreBefore - scoreAfter).toDouble() / scoreBefore.toDouble() * 100,
                                        "testingScore" to sendObject.getInteger("scoreCompare", -1),
                                        "subject" to mailsubject

                                    )
                                )


                                val html = templateEngine.process("notification", ctx)

                                //piveauLog.info(html)

                                val mailMessage = MailMessage()
                                with(mailMessage) {
                                    to = mailList
                                    from = fromAddress
                                    subject = mailsubject
                                    inlineAttachment = listOf(logoAttachement, arrowDownAttachement, arrowUpAttachement)
                                    this.html = html
                                }
                                if (!mailCopyTo.isEmpty) {
                                    mailMessage.bcc = mailCopyTo.list as MutableList<String>?
                                }

                                // so that we do not accidentally send someone mails when testing, the env var
                                // IN_TEST_ENVIRONMENT has to be explicitly set to false, when we want to send mails
                                if (config.getBoolean(ENV_IN_TEST_ENVIRONMENT, DEFAULT_IN_TEST_ENVIRONMENT)) {

                                    piveauContext.extend(id).log()
                                        .info("Debug Testmail, not sent: ${mailMessage.toJson().encodePrettily()}")

                                } else {

                                    breaker.execute<MailResult> {
                                        piveauLog.info("Sending mail")
                                        mailClient.sendMail(mailMessage)
                                    }.onFailure {
                                        piveauContext.extend(id).log().warn("Could not send mail", it)
                                        message.fail(500, "Could not send mail: ${it.message}")
                                    }.onSuccess {
                                        piveauContext.extend(id).log().info("Mail sent")
                                    }

                                }


                            } else {
                                piveauContext.extend(id).log().warn("mail list is empty")
                            }

                        }


                }
            }

    }

    /**
     * get list of mail recipients from the database, remove the ones that are blacklisted
     * @param id the id of the catalogue
     * @param mailrecipient a single recipient that overwrites the list from the catalogue
     * @return a future with the list of mail addresses
     */
    private fun getMaillist(id: String, mailrecipient: String? = null): Future<MailList> =
        Promise.promise<MailList>().apply {
            val mailList = mutableListOf<String>()


            val graphUri = DCATAPUriSchema.applyFor(id).catalogueUriRef

            piveauLog.info("graphUri is $graphUri")
            val query =
                "SELECT ?mail ?dcat ?name ?name2 WHERE { " +
                        "<$graphUri> a <${DCAT.Catalog}>; <${DCTerms.publisher}> ?p .  " +
                        "OPTIONAL { <$graphUri> <${DCTerms.type}> ?dcat.  } " +
                        "OPTIONAL { ?p <${FOAF.mbox}> ?mail.} " +
                        "OPTIONAL {?p <${FOAF.name}> ?name . } " +
                        "OPTIONAL {?p <${SKOS.prefLabel}> ?name2.} " +
                        "FILTER (lang(?name2) = 'en' || lang(?name2) = '') " +
                        "}"

            piveauLog.debug(query)
            tripleStore.select(query)
                .onFailure {
                    piveauContext.extend(id).log().warn("Could not get info from Triplestore", it)
                    fail("Could not get info from Triplestore: ${it.message}")
                }
                .onSuccess { resultSet ->

                    if (resultSet.hasNext()) {
                        piveauContext.extend(id).log().debug("Got a result from triplestore")
                    } else {
                        piveauContext.extend(id).log().warn("Got no results from triplestore")
                    }


                    var isDcat = false
                    var name = ""
                    resultSet.forEachRemaining { solution: QuerySolution ->

                        if (solution.contains("mail")) {
                            val mail = solution.get("mail").asResource().uri.drop(7)
                            piveauContext.extend(id).log().info("Got mail: $mail")
                            mailList.add(mail)

                        } else {
                            piveauContext.extend(id).log().warn("no mail: $solution")
                        }

                        if (solution.contains("dcat")) {
                            val typ = solution.get("dcat").asLiteral().string
                            isDcat = (typ == "dcat-ap") || isDcat

                            piveauContext.extend(id).log().info("dct:type is: $typ")

                        } else {
                            piveauContext.extend(id).log().warn("no dct:type : $solution")
                        }

                        if (name.isEmpty()) {
                            if (solution.contains("name")) {
                                name = solution.get("name").asLiteral().string
                                piveauContext.extend(id).log().info("name is: $name")
                            } else if (solution.contains("name2")) {
                                name = solution.get("name2").asLiteral().string
                                piveauContext.extend(id).log().info("name2 is: $name")
                            } else {
                                piveauContext.extend(id).log().debug("no name: $solution")
                            }
                        }

                    }





                    getCatalogueBlacklistFromDB(id).onComplete {

                        if(it.succeeded()) {
                            it.result().forEach { blitem -> mailList.remove(blitem as String) }
                            piveauLog.info("blacklist: ${it.result().encodePrettily()}")
                            piveauContext.extend(id).log().info("mailList is: ${mailList.joinToString()}")
                        }else{
                            piveauContext.extend(id).log().warn("Could not get blacklisted addresses from DB")
                        }

                        if (!mailrecipient.isNullOrEmpty()) {
                            mailList.clear()
                            mailList.add(mailrecipient)
                            piveauContext.extend(id).log().info("Overwriting mail list with $mailrecipient")
                        }

                        complete(MailList(mailList, isDcat, name))
                    }




                }


        }.future()


    /**
     * Replace or insert catalogue in db, wrapper function for sending new/updated catalogues to databaseVerticle
     *
     * @param catalogue the catalogue to store/update
     * @param measureDate the date of the measurement
     * @return result message from Database Vertivle
     */
    private fun replaceOrInsertCatalogueInDB(catalogue: JsonObject, measureDate: LocalDate): Future<JsonObject> =
        Promise.promise<JsonObject>().apply {

            vertx.eventBus()
                .request<JsonObject>(
                    DatabaseVerticle.ADDRESS_REPLACE_OR_INSERT_IN_DB,
                    ReplaceOrInsertOneMessage(
                        DBCollection.CATALOGUE,
                        catalogue.getJsonObject("info", JsonObject()).getString("id"),
                        catalogue.put("date", measureDate.toString())
                    )

                ) { request ->

                    if (request.succeeded()) {
                        complete(request.result().body())

                    } else {
                        fail(request.cause())

                    }

                }

        }.future()

    /**
     * Get historic catalogues for a specific date from cache.
     * Wrapping getCataloguesFromCache()
     *
     * @param lastRunDate the Date
     * @return array of catalogues as JsonObjects
     */
    private fun getHistoricCataloguesFromCache(lastRunDate: LocalDate): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {

            getCataloguesFromCache(
                historicCataloguesRequest.copy()
                    .addQueryParam("startDate", lastRunDate.toString())
                    .addQueryParam("endDate", lastRunDate.toString())
                    .addQueryParam("resolution", "day")
            )

                .onSuccess { complete(it) }
                .onFailure { fail(it) }

        }.future()

    /**
     * Get one historic catalogue from cache.
     * Wrapping getCataloguesFromCache()
     *
     * @param catalogue the name of the catalogue
     * @param lastRunDate the date
     * @return array with only this catalogue as JsonObjects
     */
    private fun getOneHistoricCatalogueFromCache(catalogue: String, lastRunDate: LocalDate): Future<JsonObject> =
        Promise.promise<JsonObject>().apply {


            getCataloguesFromCache(
                client.getAbs("$address/catalogues/$catalogue/history")
                    .putHeader(HttpHeaders.ACCEPT.toString(), IANATYPE_APPLICATION_JSON)
                    .addQueryParam("startDate", lastRunDate.toString())
                    .addQueryParam("endDate", lastRunDate.toString())
                    .addQueryParam("resolution", "day")
            )

                .onSuccess {
                    if (it == null || it.isEmpty) {
                        fail("Catalogue not found in cache")
                    } else {
                        complete(it.getJsonObject(0))
                    }

                }
                .onFailure { fail(it) }

        }.future()


    /**
     * Get catalogues from cache, deciding if we want all or just one specific catalogue.
     *
     * @param catalogue the name of the catalogue
     * @return array of catalogues as JsonObjects
     */
    private fun getCurrentCataloguesFromCache(catalogue: String? = null): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {

            if (catalogue.isNullOrEmpty()) {
                getAllCurrentCataloguesFromCache().onSuccess { complete(it) }.onFailure { fail(it) }
            } else {
                getOneCurrentCatalogueFromCache(catalogue).onSuccess { complete(JsonArray().add(it)) }
                    .onFailure { fail(it) }
            }

        }.future()


    /**
     * Get one catalogue from cache.
     * Wrapping getCataloguesFromCache()
     *
     * @param catalogue the name of the catalogue
     * @return array with only this catalogue as JsonObjects
     */
    private fun getOneCurrentCatalogueFromCache(catalogue: String): Future<JsonObject> =
        Promise.promise<JsonObject>().apply {


            getCataloguesFromCache(
                client.getAbs("$address/catalogues/$catalogue")
                    .putHeader(HttpHeaders.ACCEPT.toString(), IANATYPE_APPLICATION_JSON)
            )

                .onSuccess {
                    if (it.isEmpty) {
                        fail("Catalogue not found in cache")
                    } else {
                        complete(it.getJsonObject(0))
                    }
                }
                .onFailure { fail(it) }

        }.future()


    /**
     * Get current catalogues from cache
     * Wrapping getCataloguesFromCache()
     *
     * @return array of catalogues as JsonObjects
     */
    private fun getAllCurrentCataloguesFromCache(): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {

            getCataloguesFromCache(currentCataloguesRequest.copy())
                .onSuccess { complete(it) }
                .onFailure { fail(it) }
        }.future()


    /**
     * Get catalogues from cache. Helper function for communication with cache
     *
     * @param request
     * @return
     */
    private fun getCataloguesFromCache(request: HttpRequest<Buffer>): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {

            breaker.execute<HttpResponse<Buffer>> { promise ->

                request.copy().send {
                    if (it.succeeded()) {
                        if (it.result().statusCode() == 404) {
                            promise.fail(it.result().statusMessage())
                        } else {
                            promise.complete(it.result())
                        }
                    } else {
                        promise.fail(it.cause())
                    }
                }
            }.onSuccess {
                when (it.statusCode()) {
                    in 200..299 -> {

                        val jsonBody = it.bodyAsJsonObject()
                        if (jsonBody.containsKey("success") && jsonBody.getBoolean("success")) {

                            complete(
                                jsonBody.getJsonObject("result", JsonObject()).getJsonArray("results", JsonArray())
                            )
                        } else {
                            piveauLog.error(jsonBody.encodePrettily())
                            fail(jsonBody.encodePrettily())
                        }
                    }

                    500 -> {
                        val reason = it.bodyAsString()?.let { body ->
                            if (body.isNotBlank()) {
                                piveauLog.error(body)
                                body.substringBefore("\\n")
                            } else {
                                it.statusMessage()
                            }
                        } ?: it.statusMessage()
                        fail(reason)
                    }

                    else -> fail(it.statusMessage())
                }
            }.onFailure { fail(it) }

        }.future()


    /**
     * Get catalogues from DB. Helper function  to decide if only one or all catalogues should be returned
     * @param catalogue the name of the catalogue, empty, if all should be returned
     * @return array of catalogues as JsonObjects
     */
    private fun getCataloguesFromDB(catalogue: String): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {

            if (catalogue.isBlank()) {
                getAllCataloguesFromDB().onSuccess { complete(it) }
                    .onFailure { fail(it) }
            } else {
                getCatalogueFromDB(catalogue).onSuccess { complete(it) }
                    .onFailure { fail(it) }
            }
        }.future()


    /**
     * Get catalogue from DB.
     * @param catalogue the name of the catalogue
     * @return array with the catalogue as JsonObjects
     */
    private fun getCatalogueFromDB(catalogue: String): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {

            vertx.eventBus()
                .request<JsonArray>(
                    DatabaseVerticle.ADDRESS_GETONE_FROM_DB,
                    OneCatalogueMessage(
                        DBCollection.CATALOGUE,
                        catalogue
                    )

                ) { request ->

                    if (request.succeeded()) {
                        complete(request.result().body())

                    } else {
                        fail(request.cause())

                    }

                }

        }.future()


    /**
     * Get all catalogues from db. Requests all catalogues stored in the internal db from the DatabaseVerticle
     *
     * @return array of catalogues as JsonObjects
     */
    private fun getAllCataloguesFromDB(): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {


            val msg = GetAllMessage(DBCollection.CATALOGUE)
            vertx.eventBus().request<JsonArray>(DatabaseVerticle.ADDRESS_GETALL_FROM_DB, msg) {
                if (it.succeeded()) {
                    complete(it.result().body())
                } else {
                    fail(it.cause())
                }
            }

        }.future()

    private fun getCatalogueBlacklistFromDB(catalogue: String): Future<JsonArray> =
        Promise.promise<JsonArray>().apply {


            val msg = OneCatalogueMessage(DBCollection.USER, catalogue)
            vertx.eventBus().request<JsonArray>(DatabaseVerticle.ADDRESS_GETONE_FROM_DB, msg) {
                if (it.succeeded()) {
                    if (it.result().body().isEmpty) {
                        fail("No Elements in response")
                    } else {
                        complete(it.result().body().getJsonObject(0).getJsonArray("blacklist", JsonArray()))
                    }

                } else {
                    fail(it.cause())
                }
            }

        }.future()


    /**
     * Compare yes no compares two measurements for the same metric for changes
     *
     * @param old the older measurement
     * @param new the newer measurement
     * @return a ComparedPercentage Object indicating changes
     */
    private fun compareYesNo(old: JsonArray?, new: JsonArray?): ComparedPercentage =
        ComparedPercentage().apply {


            if (new != null && old != null) {
                var oldval = 0
                old.forEach {
                    if ((it as JsonObject).getString("name", "")!!.contentEquals("yes")) {
                        oldval = it.getInteger("percentage")
                    }
                }

                new.forEach {
                    if ((it as JsonObject).getString("name", "")!!.contentEquals("yes")) {
                        val newval = it.getInteger("percentage")
                        this.value = newval
                    }
                }

                if (value != null) {
                    down = oldval > value!!
                    up = value!! > oldval
                }


            } else {
                if (old != null) {
                    this.down = true
                }
            }

        }

    /**
     * Compare statuscodes compares two statuscode measurements for the same metric for changes
     *
     * @param old the older measurement
     * @param new the newer measurement
     * @return a ComparedStatusCode Object indicating changes
     */
    private fun compareStatuscodes(old: JsonArray?, new: JsonArray?): ComparedStatusCode =
        ComparedStatusCode().apply {


            if (new != null && old != null) {


                var newMostCode = ""
                var newMostVal = 0
                new.forEach {
                    if ((it as JsonObject).getInteger("percentage", 0) > newMostVal) {
                        newMostVal = it.getInteger("percentage")
                        newMostCode = it.getString("name")
                    }
                }

                old.forEach {
                    if ((it as JsonObject).getString("name", "")!!.contentEquals(newMostCode)) {
                        this.down = it.getInteger("percentage") > newMostVal
                        this.up = newMostVal > it.getInteger("percentage")
                    }
                }
                value = newMostCode


            } else {
                if (old != null) {
                    down = true
                }
            }

        }


    private fun getComparedField(field: String, old: JsonObject, new: JsonObject): ComparedPercentage =
        compareYesNo(old.getJsonArray(field), new.getJsonArray(field))

    /**
     * Get compared data compares the same catalogue measured at two different points of time and returns an object indicating where changes occured
     *
     * @param old the older catalogue
     * @param new the newer catalogue
     * @return a ComparedCatalogue object indicating changes
     */
    private fun getComparedData(old: JsonObject, new: JsonObject): ComparedCatalogue =
        ComparedCatalogue(

            accessibility = Accessibility().apply {
                val oldAcc = old.getJsonObject("accessibility", JsonObject())
                val newAcc = new.getJsonObject("accessibility", JsonObject())

                accessUrlStatusCodes = compareStatuscodes(
                    oldAcc.getJsonArray("accessUrlStatusCodes", JsonArray()),
                    newAcc.getJsonArray("accessUrlStatusCodes", JsonArray())

                )
                downloadUrlStatusCodes = compareStatuscodes(
                    oldAcc.getJsonArray("downloadUrlStatusCodes", JsonArray()),
                    newAcc.getJsonArray("downloadUrlStatusCodes", JsonArray())

                )
                downloadUrlAvailability = compareYesNo(
                    oldAcc.getJsonArray("downloadUrlAvailability", JsonArray()),
                    newAcc.getJsonArray("downloadUrlAvailability", JsonArray())

                )
            },
            contextuality = Contextuality().apply {
                val oldAcc = old.getJsonObject("contextuality", JsonObject())
                val newAcc = new.getJsonObject("contextuality", JsonObject())


                byteSizeAvailability = getComparedField("byteSizeAvailability", oldAcc, newAcc)
                dateIssuedAvailability = getComparedField("dateIssuedAvailability", oldAcc, newAcc)
                dateModifiedAvailability = getComparedField("dateModifiedAvailability", oldAcc, newAcc)
                rightsAvailability = getComparedField("rightsAvailability", oldAcc, newAcc)
            },
            findability = Findability().apply {
                val oldAcc = old.getJsonObject("findability", JsonObject())
                val newAcc = new.getJsonObject("findability", JsonObject())


                temporalAvailability = getComparedField("temporalAvailability", oldAcc, newAcc)
                spatialAvailability = getComparedField("spatialAvailability", oldAcc, newAcc)
                keywordAvailability = getComparedField("keywordAvailability", oldAcc, newAcc)
                categoryAvailability = getComparedField("categoryAvailability", oldAcc, newAcc)

            },
            interoperability = Interoperability().apply {
                val oldAcc = old.getJsonObject("interoperability", JsonObject())
                val newAcc = new.getJsonObject("interoperability", JsonObject())

                dcatApCompliance = getComparedField("dcatApCompliance", oldAcc, newAcc)
                formatAvailability = getComparedField("formatAvailability", oldAcc, newAcc)
                formatMediaTypeAlignment = getComparedField("formatMediaTypeAlignment", oldAcc, newAcc)
                formatMediaTypeMachineReadable =
                    getComparedField("formatMediaTypeMachineReadable", oldAcc, newAcc)
                formatMediaTypeNonProprietary =
                    getComparedField("formatMediaTypeNonProprietary", oldAcc, newAcc)
                mediaTypeAvailability = getComparedField("mediaTypeAvailability", oldAcc, newAcc)
            },
            reusability = Reusability().apply {
                val oldAcc = old.getJsonObject("reusability", JsonObject())
                val newAcc = new.getJsonObject("reusability", JsonObject())


                accessRightsAlignment = getComparedField("accessRightsAlignment", oldAcc, newAcc)
                accessRightsAvailability = getComparedField("accessRightsAvailability", oldAcc, newAcc)
                contactPointAvailability = getComparedField("contactPointAvailability", oldAcc, newAcc)
                licenceAlignment = getComparedField("licenceAlignment", oldAcc, newAcc)
                licenceAvailability = getComparedField("licenceAvailability", oldAcc, newAcc)
                publisherAvailability = getComparedField("publisherAvailability", oldAcc, newAcc)
            })


    /**
     * Collect send set retrieves old catalogues from the internal db and new catalogues from the metrics cache
     * it compares the score of each new catalogue with the matching old one.
     *
     * When no old catalogue is present in the internal database, it tries to fetch it from the metrics cache.
     *
     * When the score went down, the catalogue is added to the return Set to notify the responsible parties.
     *
     *
     * @param lastDate the date of the last run
     * @return a set containg all catalogues with worse scores than at last run
     */
    private fun collectSendSet(
        lastDate: LocalDate,
        catalogue: String = "",
        scoreCompare: Int? = null
    ): Future<Set<JsonObject>> =
        Promise.promise<Set<JsonObject>>().apply {
            piveauLog.info("Collecting send set for $catalogue")
            getCataloguesFromDB(catalogue).onFailure { fail(it) }
                .onSuccess { jsonArray ->
                    val catMap = HashMap<String, JsonObject>().apply {
                        jsonArray.iterator().forEach {
                            this[(it as JsonObject).getJsonObject("info", JsonObject()).getString("id")] = it
                        }
                    }

                    val sendSet: HashSet<JsonObject> = HashSet()

                    getCurrentCataloguesFromCache(catalogue).onFailure { fail(it) }.onSuccess { array ->

                        piveauLog.info("active catalogues are: $activeCatalogues")
                        piveauLog.info("single catalogue modus: ${catalogue.isNotEmpty()}")
                        if (catalogue.isNotEmpty()) piveauLog.info("single catalogue: $catalogue")


                        array.iterator().forEach { listObject ->
                            with(listObject as JsonObject) {

                                val scoreNow = this.getInteger("score")
                                val id = this.getJsonObject("info", JsonObject()).getString("id")
                                var catThen = catMap[id]


                                //ignore this catalogue, if we have a list of active catalogues and this one is not in it
                                if (!activeCatalogues.contains(id) && catalogue != id) {
                                    piveauLog.info("send set check id: $id, is active: no")
                                    return@with
                                }
                                piveauLog.info("send set check id: $id, is active: yes")


                                //If we do not have an old one, lets try to get it
                                if (catThen == null) {
                                    val res = getOneHistoricCatalogueFromCache(
                                        this.getJsonObject("info", JsonObject()).getString("id"), lastDate
                                    ).result() ?: JsonObject()

                                    catThen = remorphDateArrays(res)

                                }


                                var scoreThen = catThen.getInteger("score", 0)
                                if (scoreCompare != null) {
                                    scoreThen = scoreCompare
                                    catThen.put("score", scoreThen)
                                }

                                val catLog = piveauContext.extend(id).log()

                                catLog.info("old score: $scoreThen, new score: $scoreNow")
                                // piveauContext.extend(this.getJsonObject("info", JsonObject()).getString("id")).log().info("old catalogue: ${catThen.encodePrettily()}")
                                if (scoreThen != 0 && scoreNow < scoreThen - scoredelta) { // we got a old score but it was much higher -> save new score & notify
                                    val sendJson = JsonObject().put("new", listObject).put("old", catThen)
                                    if (scoreCompare != null) {
                                        sendJson.put("scoreCompare", scoreCompare)
                                    }

                                    sendSet.add(sendJson)

                                    copyNewestCatalogueFromCacheIntoDB(id).onFailure {
                                        catLog.error(
                                            it.toString()
                                        )
                                    }.onSuccess { catLog.info("Catalogue updated in DB: old score was higher") }

                                } else if (scoreThen == 0 && scoreNow > 0) { // there was no score in database, but there is one in cache now-> save to db
                                    copyNewestCatalogueFromCacheIntoDB(id).onFailure {
                                        catLog.error(
                                            it.toString()
                                        )
                                    }.onSuccess { catLog.info("Catalogue updated in DB: old score not available") }
                                } else if (scoreNow > scoreThen) { // new score is higher than old score -> save new score
                                    copyNewestCatalogueFromCacheIntoDB(id).onFailure {
                                        catLog.error(
                                            it.toString()
                                        )
                                    }.onSuccess { catLog.info("Catalogue updated in DB: old score was lower") }
                                }

                            }

                        }

                        complete(sendSet)
                    }


                }
        }.future()

    /**
     * Copy historic catalogue from cache into db. For faster internal access.
     *
     * @param id the catalogue ID
     * @return the answer from the databse verticle as JsonObject
     */
    private fun copyNewestCatalogueFromCacheIntoDB(id: String): Future<JsonObject> =
        Promise.promise<JsonObject>().apply {

            val date = LocalDate.now()

            getOneHistoricCatalogueFromCache(id, date).onFailure {
                fail(it)
            }.onSuccess { cat ->

                if (cat.getJsonArray("score").isEmpty) {

                    getOneCurrentCatalogueFromCache(id).onFailure { fail(it) }.onSuccess { catalogueJson ->
                        replaceOrInsertCatalogueInDB(catalogueJson, date).onFailure { fail(it) }
                            .onSuccess { complete(it) }


                    }

                } else {

                    replaceOrInsertCatalogueInDB(remorphDateArrays(cat), date).onFailure { fail(it) }
                        .onSuccess { complete(it) }

                }

            }

        }.future()


    /**
     * remorphDateArrays morphs an historic representation of an catalogue with date arrays into a normal representation without date arrays
     *
     * @param histCat a historic catalogue with date arrays
     * @return the same catalogue without date arrays
     */
    private fun remorphDateArrays(histCat: JsonObject): JsonObject = JsonObject().apply {


        histCat.fieldNames().forEach { field ->

            if (field == "info") {
                this.put("info", histCat.getJsonObject("info"))

            } else if (field == "score") { //score is different than other dimensions
                if (!histCat.getJsonArray(field).isEmpty) {


                    put(
                        field,
                        histCat.getJsonArray(field).getJsonObject(histCat.getJsonArray(field).size() - 1).first().value
                    )
                }
            } else {
                val histDim = histCat.getJsonObject(field)

                val dim = JsonObject().apply {
                    histDim.fieldNames().forEach { metric ->

                        if (!histDim.getJsonArray(metric, JsonArray()).isEmpty) {


                            this.put(
                                metric,
                                histDim.getJsonArray(metric).getJsonObject(histDim.getJsonArray(metric).size() - 1)
                                    .first().value
                            )
                        }
                    }
                }
                put(field, dim)


            }
        }

    }


}