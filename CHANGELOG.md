# ChangeLog

# Unreleased

## 1.0.13 (2024-08-05)
**Added:**
* mail server connection documentation

## 1.0.12 (2024-08-05)
**Fixed**
* From Address fix: [#12](https://gitlab.com/piveau/community/issues/-/issues/12)

## 1.0.11 (2024-07-17)

**Changed:** 
* Mail sending used breaker for better error handling
* Changed wording in mail

**Fixed:**
* possible NullPointerException when a metric is null

## 1.0.10 (2024-01-22)
**JUST for publish to piveau gitlab.com**


## 1.0.9 (2022-10-11)

**Fixed**
* Typo in mail footer


## 1.0.8 (2022-10-11)

**Fixed:**
* added slash to the end of the Catalogue detail base url if it is missing
* updated link to feedback form

**Changed:**
* updated mail template text

## 1.0.7 (2022-06-23)

**Added:**
* enpoint for force triggering a check for a single catalogue

## 1.0.6 (2021-10-18)

**Changed:**
* Extended DCAT-AP schema configuration

## 1.0.5 (2021-09-20)

**Added:**
* Mail config per secret

**Changed:**
* Logo as PNG and increased in size

**Fixed:**
* Default JsonObject when history not available

## 1.0.4 (2021-09-09)

**Changed:**
* Replaced finally all EDP to data.europa.eu

**Fixed:**
* Wrong config for mongo db

## 1.0.3 (2021-06-07)

**Fixed**
* Housekeeping

## 1.0.2 (2021-5-12)

**Added:**
* Replaced EDP and European Data Portal with data.europa.eu
* Replaced old EDP logo with new data.europa.eu logo in e-mail template
* Changed old colours (Interoperability, Accessibility, Reusability, Contextuality, Findability) to new colours in e-mail template

## 1.0.1 (2021-03-05)

**Added:**
* Supporting configurable favicon and logo

## 1.0.0 (2021-02-17)

???

## 0.0.1 (2020-12-03)

Initial release
