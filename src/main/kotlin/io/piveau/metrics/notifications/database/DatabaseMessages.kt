package io.piveau.metrics.notifications.database

import io.piveau.metrics.notifications.utils.DBCollection
import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.MessageCodec
import io.vertx.core.json.JsonObject

data class GetAllMessage(val collection: DBCollection)
data class ReplaceOrInsertOneMessage(
    val collection: DBCollection,
    val catalogue: String,
    val document: JsonObject
)

data class OneCatalogueMessage(
    val collection: DBCollection,
    val catalogue: String
)

data class BlacklistMessage(
    val catalogue: String,
    val mail: String
)


class GetAllMessageCodec : MessageCodec<GetAllMessage, GetAllMessage> {

    override fun transform(getAllMessage: GetAllMessage?): GetAllMessage? = getAllMessage
    override fun name(): String = this.javaClass.simpleName
    override fun systemCodecID(): Byte = -1

    //not needed for local messaging
    override fun encodeToWire(buffer: Buffer?, s: GetAllMessage?) {
        TODO("Not yet implemented")
    }

    override fun decodeFromWire(pos: Int, buffer: Buffer?): GetAllMessage {
        TODO("Not yet implemented")
    }
}

class OneCatalogueMessageCodec : MessageCodec<OneCatalogueMessage, OneCatalogueMessage> {

    override fun transform(oneCatalogueMessage: OneCatalogueMessage?): OneCatalogueMessage? = oneCatalogueMessage
    override fun name(): String = this.javaClass.simpleName
    override fun systemCodecID(): Byte = -1

    //not needed for local messaging
    override fun encodeToWire(buffer: Buffer?, s: OneCatalogueMessage?) {
        TODO("Not yet implemented")
    }

    override fun decodeFromWire(pos: Int, buffer: Buffer?): OneCatalogueMessage {
        TODO("Not yet implemented")
    }
}


class ReplaceOrInsertOneMessageCodec : MessageCodec<ReplaceOrInsertOneMessage, ReplaceOrInsertOneMessage> {
    override fun encodeToWire(buffer: Buffer?, s: ReplaceOrInsertOneMessage?) {
        TODO("Not yet implemented")
    }

    override fun decodeFromWire(pos: Int, buffer: Buffer?): ReplaceOrInsertOneMessage {
        TODO("Not yet implemented")
    }

    override fun transform(s: ReplaceOrInsertOneMessage?): ReplaceOrInsertOneMessage? = s
    override fun name(): String = this.javaClass.simpleName
    override fun systemCodecID(): Byte = -1
}

class BlacklistMessageCodec : MessageCodec<BlacklistMessage, BlacklistMessage> {
    override fun encodeToWire(buffer: Buffer?, s: BlacklistMessage?) {
        TODO("Not yet implemented")
    }

    override fun decodeFromWire(pos: Int, buffer: Buffer?): BlacklistMessage {
        TODO("Not yet implemented")
    }

    override fun transform(s: BlacklistMessage?): BlacklistMessage? = s
    override fun name(): String = this.javaClass.simpleName
    override fun systemCodecID(): Byte = -1
}